import { login } from "./api.js";
import dateUtil from "@/utils/dateUtil.js";

import {
  SESSION_STORAGE_KEY,
  SESSION_STORAGE_ACCOUNT,
  SESSION_STORAGE_BADGE
} from "@/utils/const";

export default {
  // 是否登录
  isLogin: function() {
    return this.curUser() != null;
  },
  // 当前用户信息
  curUser: function() {
    var data = sessionStorage.getItem(SESSION_STORAGE_KEY.USER);

    if (
      data == null ||
      data == "null" ||
      data == undefined ||
      data == "undefined" ||
      data.trim() == ""
    ) {
      return null;
    }
    return JSON.parse(data);
  },
  // 得到cooke
  getCookie: function(cname) {
    let cend;

    if (document.cookie.length > 0) {
      let cstart = document.cookie.indexOf(cname + "=");
      if (cstart != -1) {
        cstart = cstart + cname.length + 1;
        cend = document.cookie.indexOf(";", cstart);
        if (cend == -1) cend = document.cookie.length;
        return unescape(document.cookie.substring(cstart, cend));
      }
    }
    return "";
  },
  // 登录，获取用户信息
  login: function(token, fun) {
    return login({ token: token }).then(res => {
      if (res.code == 0) {
        // 设置用户信息
        this.setUserInfo(res.user, res);

        // 登录成功
        if (fun) {
          fun();
        }
      } else {
        // 登录失败，则页面跳转失败
        // Toast.fail(res.msg ? res.msg : "您暂无权限访问");
      }
    });
  },
  // 身份认证
  routerNext(to, from, next) {
    // 登录状态
    const islogin = this.isLogin();
    if (!islogin) {
      const url = window.location.href;
      var token =
        process.env.SERVER_ENV === "dev" ||
        ((process.env.SERVER_ENV === "test" ||
          url.indexOf("/mobileuat/#") > -1) &&
          !this.getCookie("wechat-token") &&
          ((dd && dd.env.platform != "notInDingTalk") ||
            url.indexOf("?debug") > -1))
          ? "qSv04K0ObcOrrmLoXufItjaogmqvy5R9s8eXYnd5pa9JVyVcAt5YHBP5Pj-UIubELr1tserghC5aWhE0ox58tbibEUbaHnUs0DmrjCqOI84UpvGrId6qQ62bDV4IzwpQTQ9vyBrpwYwGL82_xd654BexmF0ELniu-GYegq5ui-f6G4uyIORBXwkNCTM-ojcf7MuNnj28A1zVUb8SmedBxsmYbnKcbXYYai4Ut6Hk4O6XLBpptbMusbqw6q1K8FbWCC4yoFjhJuggYxn4QvHdPSvUbJo5YIx0cr3gYBNnp8u6E30DWdkygtkXskwXsZwHHZ5YCF_C-QdD05tuDTG_6qWf-b9ed3R_mf8Js2nI62vEHraDUQBnDg"
          : this.getCookie("wechat-token");

      if (token) {
        // 有token
        this.login(token, function() {
          next();
        });
      } else {
        // 没有token，则页面跳转失败
        // Toast.fail("您暂无权限访问");
      }
    } else {
      if (to.fullPath.indexOf("?token") > -1) {
        next("/");
      } else {
        next();
      }
    }
  },
  /**
   * sessionStorage保存用户相关信息
   * @param {*} user
   * @param {*} res
   */
  setUserInfo: function(user, res) {
    //设置userId值，方便兼容指标库相关代码
    user ? (user.userId = user.account) : "";
    // 设置用户信息
    sessionStorage.setItem(SESSION_STORAGE_KEY.USER, JSON.stringify(user));
    // 设置服务器时间
    sessionStorage.setItem(
      SESSION_STORAGE_KEY.SERVERDATE,
      res.serverDate
        ? res.serverDate
        : dateUtil.format(new Date(), "yyyy-MM-dd hh:mm:ss")
    );
    // 记录更新时间(请求返回401时，判断是否需要刷新用户信息)
    sessionStorage.setItem(
      SESSION_STORAGE_KEY.UPDATE_TIME,
      new Date().getTime()
    );

    sessionStorage.setItem(SESSION_STORAGE_ACCOUNT, user.userId);

    //获取工号
    sessionStorage.setItem(SESSION_STORAGE_BADGE, user.badge);
  }
};
