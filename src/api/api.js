import axios from "./http.js";

var domainUrl = window.location.host;
if (domainUrl == undefined || domainUrl == "") {
  domainUrl = document.domain;
}

var domainProd = "https://bis.auxgroup.com";
var domainTest = "https://bis-qas.auxgroup.com";

if (domainUrl.indexOf("auxgroup.com") >= 0) {
  domainProd = "https://" + domainUrl;
  domainTest = "https://" + domainUrl;
}

//开发环境使用proxyTable做了转发代理(解决loginForMobile登录接口、经营雷达接口等开发环境跨域问题)
//在config/index.js中配置了“proxyApi”、“uatProxyApi”对应的真实服务器地址
export const CONSOLE_BASE_URL =
  process.env.SERVER_ENV === "prod"
    ? domainProd
    : process.env.SERVER_ENV === "test"
    ? domainTest
    : "/proxyApi";

// 平台管理接口地址
export const CONSOLE_PLATFORM_URL = CONSOLE_BASE_URL + "/admin";

// 预警管理接口地址
export const CONSOLE_EWS_URL = CONSOLE_BASE_URL + "/api/ews";

// 基础类api
export const CONSOLE_ARS_URL = CONSOLE_BASE_URL + "/ars";

//查看报表/指标注册信息地址
export const REPORT_REGISTER_INFO_URL =
  process.env.SERVER_ENV === "prod"
    ? "https://bi.auxgroup.com/portal"
    : "https://biqas.auxgroup.com/portal";

// 平台管理接口 开始----------

// 登录
export const login = params => {
  return axios
    .post(CONSOLE_BASE_URL + "/admin/user/loginForMobile", params)
    .then(res => {
      return res.data;
    });
};
