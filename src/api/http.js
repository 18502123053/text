import axios from "axios";
import auth from "./auth";

// 统一全局配置

// 添加一个请求拦截器
axios.interceptors.request.use(
  function (config) {
    // if (auth.isLogin()) {
    //   config.headers.token = auth.curUser().token;
    // }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// 添加一个响应拦截器
axios.interceptors.response.use(
  res => {
    return res;
  },
  err => {
    if (err === "Error: Network Error" || !err.response) {
      return Promise.resolve({
        data: { code: 500, msg: err.message || "服务器异常，请稍后再试！" }
      });
    }

    switch (err.response.status) {
      case 500:
        // Toast.fail("服务器异常，请稍后再试！");
        break;
      case 401:
        // Toast.fail("操作失败，没有权限！");

        break;
      case 403:
        // Toast.fail("操作失败，没有权限！");

        break;
    }

    // 出错时自定义返回内容
    return Promise.resolve({
      data: { code: 500, msg: "服务器异常，请稍后再试！" }
    });
    // return Promise.reject(err);
  }
);

export default axios;
