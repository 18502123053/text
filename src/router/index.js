/*
 * @Author: gonghairun
 * @Date: 2021-12-08 18:27:24
 * @LastEditors: gonghairun
 * @LastEditTime: 2021-12-14 13:28:11
 * @Description:
 */
import Vue from "vue";
import Router from "vue-router";
import auth from "../api/auth";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: "/",
            redirect: "/Home",
            meta: { title: "BOSS大屏" }
        },
        {
            // 首页
            path: "/Home",
            component: resolve => require(["@/views/home-page"], resolve),
            meta: { title: "BOSS大屏", keepAlive: true }
        },
        {
            // 家内-营收-首页
            path: "/JN/ying-shou",
            component: resolve =>
                require(["@/views/jia-nei/ying-shou"], resolve),
            meta: { title: "家内营收", keepAlive: true }
        },
        {
            // 家内-营收-首页
            path: "/revenue",
            component: resolve => require(["@/views/revenue"], resolve),
            meta: { title: "家内营收", keepAlive: true }
        },
        {
            // 家内-营收-首页
            path: "/revenueSecond",
            component: resolve => require(["@/views/revenueSecond"], resolve),
            meta: { title: "家内营收二级页面", keepAlive: true }
        },
        {
            // 家内-营收-首页
            path: "/revenueThird",
            component: resolve => require(["@/views/revenueThird"], resolve),
            meta: { title: "家内营收三级页面", keepAlive: true }
        }
    ]
});

//  使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    // 登录验证
    auth.routerNext(to, from, next);
});

router.afterEach((to, from) => {});

router.onError(error => {
    const pattern = /Loading chunk (\d)+ failed/g;
    const isChunkLoadFailed = error.message.match(pattern);
    if (isChunkLoadFailed) {
        location.reload();
    }
});

export default router;
