export default {
    install(Vue) {
        // 排序
        Vue.prototype.sortChange = function(data, prop, order) {
            let tableListBase = [],
                tableList = [];
            data.map((item, index) => {
                index == 0 ? tableListBase.push(item) : tableList.push(item);
            });
            tableList =
                order == "descending"
                    ? tableList.sort(function(a, b) {
                          return b[prop] - a[prop];
                      })
                    : order == "ascending"
                    ? tableList.sort(function(a, b) {
                          return a[prop] - b[prop];
                      })
                    : tableList;
            return [...tableListBase, ...tableList];
        };
        // 排名数据处理
        Vue.prototype.rankDataProcess = function(data, prop, key) {
            let tableListBase = [],
                tableList = [];
            data.map((item, index) => {
                index == 0 ? tableListBase.push(item) : tableList.push(item);
            });
            if (tableList.length <= 4) {
                tableList.map(item => {
                    this.$set(item, prop, "第" + item[prop]);
                });
                return [...tableListBase, ...tableList];
            } else {
                tableList.map((item, index) => {
                    console.log(prop);
                    this.$set(item, prop, "第" + (index + 1));
                });
                let value = tableList[tableList.length - 4][key];
                console.log(value);
                if (value > 100) {
                    this.$set(tableList[tableList.length - 3], prop, "后3");
                    this.$set(tableList[tableList.length - 2], prop, "后2");
                    this.$set(tableList[tableList.length - 1], prop, "后1");
                } else {
                    this.$set(tableList[tableList.length - 4], prop, "后4");
                    this.$set(tableList[tableList.length - 3], prop, "后3");
                    this.$set(tableList[tableList.length - 2], prop, "后2");
                    this.$set(tableList[tableList.length - 1], prop, "后1");
                    console.log(tableList);
                }
            }
            return [...tableListBase, ...tableList];
        };
        // 排名处理
        Vue.prototype.rankColorProcess = function(data, key) {
            let tableListBase = [],
                tableList = [];
            data.map((item, index) => {
                index == 0 ? tableListBase.push(item) : tableList.push(item);
            });
            let value = tableList[tableList.length - 4][key];
            return value > 100 ? [3, 3] : [2, 4];
        };
        // 数据处理
        Vue.prototype.getDataColor = function(data, type) {
            let color;
            if (
                data !== "" &&
                data !== null &&
                data !== undefined &&
                data !== "--"
            ) {
                var reg = /^-?[0-9]+.?[0-9]*/; //是否为数字
                //如果是数字
                if (reg.test(data)) {
                    if (type == 0) {
                        if (data >= 100) {
                            color = "reach-green";
                        } else if (data < 95) {
                            color = "reach-red";
                        } else {
                            color = "reach-yellow";
                        }
                    } else if (type == 1) {
                        if (data >= 0) {
                            color = "reach-green";
                        } else {
                            color = "reach-red";
                        }
                    }
                } else {
                    color = "reach-red";
                }
            } else {
                color = "reach-red";
            }
            return color;
        };
        // 数据处理
        Vue.prototype.dataProcess = function(data, type, typeVal) {
            let num, unit;
            if (
                data !== "" &&
                data !== null &&
                data !== undefined &&
                data !== "--"
            ) {
                // if (data.indexOf(",") != -1) {
                var reg = /^-?[0-9]+.?[0-9]*/; //是否为数字
                //如果是数字
                if (reg.test(data)) {
                    if (type === "money") {
                        if (typeVal === "tenth") {
                            // if(data>0 && data<100){
                            //     num = '<0.01'
                            //     unit = '万'
                            // }else
                            if (data == null) {
                                num = "";
                            } else {
                                num = (data / 10000).toFixed(1);
                                unit = "万";
                            }
                        } else {
                            num = (data / 10000).toFixed(1);
                            unit = "万";
                        }
                    } else if (type === "percent") {
                        if (typeVal === "chartPercent") {
                            num = (data * 100).toFixed(1);
                            unit = "%";
                        } else if (data == null) {
                            num = "";
                        } else {
                            num = (data * 100).toFixed(1);
                            unit = "%";
                        }
                    } else if (type === "") {
                        num = data;
                        unit = "";
                    } else {
                        num = (data / 1).toFixed(0);
                        unit = "";
                    }
                    return {
                        num,
                        unit
                    };
                } else {
                    num = data;
                    unit = "";
                }
            } else {
                num = "--";
                unit = "";
            }
            return {
                num,
                unit
            };
        };
        Vue.prototype.getUrlParams = function(name) {
            // 不传name返回所有值，否则返回对应值
            var url = window.location.search;
            if (url.indexOf("?") === 1) {
                return false;
            }
            url = url.substr(1);
            url = url.split("&");
            // eslint-disable-next-line no-redeclare
            var name = name || "";
            var nameres;
            // 获取全部参数及其值
            for (var i = 0; i < url.length; i++) {
                var info = url[i].split("=");
                var obj = {};
                obj[info[0]] = decodeURI(info[1]);
                url[i] = obj;
            }
            // 如果传入一个参数名称，就匹配其值
            if (name) {
                for (let i = 0; i < url.length; i++) {
                    for (const key in url[i]) {
                        if (key === name) {
                            nameres = url[i][key];
                        }
                    }
                }
            } else {
                nameres = url;
            }
            // 返回结果
            return nameres;
        };
        Vue.prototype.Base64 = function() {
            // private property
            var _keyStr =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            // public method for encoding
            this.encode = function(input) {
                var output = "";
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                var i = 0;
                input = _utf8_encode(input);
                while (i < input.length) {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);
                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;
                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }
                    output =
                        output +
                        _keyStr.charAt(enc1) +
                        _keyStr.charAt(enc2) +
                        _keyStr.charAt(enc3) +
                        _keyStr.charAt(enc4);
                }
                return output;
            };

            // public method for decoding
            this.decode = function(input) {
                var output = "";
                var chr1, chr2, chr3;
                var enc1, enc2, enc3, enc4;
                var i = 0;
                // eslint-disable-next-line no-useless-escape
                input = input.replace(/[^A-Za-z0-9\+/\=]/g, "");
                while (i < input.length) {
                    enc1 = _keyStr.indexOf(input.charAt(i++));
                    enc2 = _keyStr.indexOf(input.charAt(i++));
                    enc3 = _keyStr.indexOf(input.charAt(i++));
                    enc4 = _keyStr.indexOf(input.charAt(i++));
                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;
                    output = output + String.fromCharCode(chr1);
                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }
                }
                output = _utf8_decode(output);
                return output;
            };

            // private method for UTF-8 encoding
            var _utf8_encode = function(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if (c > 127 && c < 2048) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            };

            // private method for UTF-8 decoding
            var _utf8_decode = function(utftext) {
                var string = "";
                var i = 0;
                var c = 0;
                var c2 = 0;
                var c3 = 0;
                while (i < utftext.length) {
                    c = utftext.charCodeAt(i);
                    if (c < 128) {
                        string += String.fromCharCode(c);
                        i++;
                    } else if (c > 191 && c < 224) {
                        c2 = utftext.charCodeAt(i + 1);
                        string += String.fromCharCode(
                            ((c & 31) << 6) | (c2 & 63)
                        );
                        i += 2;
                    } else {
                        c2 = utftext.charCodeAt(i + 1);
                        c3 = utftext.charCodeAt(i + 2);
                        string += String.fromCharCode(
                            ((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)
                        );
                        i += 3;
                    }
                }
                return string;
            };
        };
        //URL加密
        Vue.prototype.Secret_Key = function(str, pwd, type) {
            var b = new this.Base64(); //Base64加密
            if (type == "encryption") {
                str = b.encode(str); //Base64加密
                var prand = "";
                for (var i = 0; i < pwd.length; i++) {
                    prand += pwd.charCodeAt(i).toString();
                }
                var sPos = Math.floor(prand.length / 5);
                var mult = parseInt(
                    prand.charAt(sPos) +
                        prand.charAt(sPos * 2) +
                        prand.charAt(sPos * 3) +
                        prand.charAt(sPos * 4) +
                        prand.charAt(sPos * 5)
                );
                var incr = Math.ceil(pwd.length / 2);
                var modu = Math.pow(2, 31) - 1;
                if (mult < 2) {
                    alert("Please choose a more complex or longer password.");
                    return null;
                }
                var salt = Math.round(Math.random() * 1000000000) % 100000000;
                prand += salt;
                while (prand.length > 10) {
                    prand = (
                        parseInt(prand.substring(0, 10)) +
                        parseInt(prand.substring(10, prand.length))
                    ).toString();
                }
                prand = (mult * prand + incr) % modu;
                var enc_chr = "";
                var enc_str = "";
                for (let i = 0; i < str.length; i++) {
                    enc_chr = parseInt(
                        str.charCodeAt(i) ^ Math.floor((prand / modu) * 255)
                    );
                    if (enc_chr < 16) {
                        enc_str += "0" + enc_chr.toString(16);
                    } else enc_str += enc_chr.toString(16);
                    prand = (mult * prand + incr) % modu;
                }
                salt = salt.toString(16);
                while (salt.length < 8) salt = "0" + salt;
                enc_str += salt;
                return enc_str;
            }
            if (type == "decryption") {
                let prand = "";
                for (let i = 0; i < pwd.length; i++) {
                    prand += pwd.charCodeAt(i).toString();
                }
                let sPos = Math.floor(prand.length / 5);
                let mult = parseInt(
                    prand.charAt(sPos) +
                        prand.charAt(sPos * 2) +
                        prand.charAt(sPos * 3) +
                        prand.charAt(sPos * 4) +
                        prand.charAt(sPos * 5)
                );
                let incr = Math.round(pwd.length / 2);
                let modu = Math.pow(2, 31) - 1;
                let salt = parseInt(
                    str.substring(str.length - 8, str.length),
                    16
                );
                str = str.substring(0, str.length - 8);
                prand += salt;
                while (prand.length > 10) {
                    prand = (
                        parseInt(prand.substring(0, 10)) +
                        parseInt(prand.substring(10, prand.length))
                    ).toString();
                }
                prand = (mult * prand + incr) % modu;
                let enc_chr = "";
                let enc_str = "";
                for (let i = 0; i < str.length; i += 2) {
                    enc_chr = parseInt(
                        parseInt(str.substring(i, i + 2), 16) ^
                            Math.floor((prand / modu) * 255)
                    );
                    enc_str += String.fromCharCode(enc_chr);
                    prand = (mult * prand + incr) % modu;
                }
                //return enc_str;
                return b.decode(enc_str);
            }
        };
    }
};
