//用于重写或者自定义一些对象的属性,例如Number的toFixed方法等
//包括如下：

//--------Vue对象相关---------------------
//1、全局定义拖拽指令drag
//2、指标对象取值的一些通用方法

//--------Number对象相关---------------------
//1、Number对象添加toUnExponential方法，用于将科学计数法转为字符串
//2、重写Number对象的toFixed方法，以满足小数部分位数不足需要补0的情况

import Vue from "vue";
import common from "./common";

//全局定义拖拽指令
Vue.directive("drag", {
  inserted: function (el, binding, vnode) {
    //如果使用自定义指令时，添加了parent修饰符，则移动的对象为父元素
    let moveObj = JSON.stringify(binding.modifiers.parent) ? el.parentNode : el;
    let disx, disy;
    common.addEvents(el, "touchstart", function (e) {
      let touchObj;
      if (e.touches) {
        //有可能对象在e上也有可能对象在e.touches[0]上
        touchObj = e.touches[0];
      } else {
        touchObj = e;
      }
      disx = touchObj.pageX - moveObj.offsetLeft;
      disy = touchObj.pageY - moveObj.offsetTop;
    });
    common.addEvents(el, "touchmove", function (e) {
      let touchObj;
      if (e.touches) {
        //有可能对象在e上也有可能对象在e.touches[0]上
        touchObj = e.touches[0];
      } else {
        touchObj = e;
      }
      if (JSON.stringify(binding.modifiers.horizonta)) {
        //仅水平拖拽
        moveObj.style.left = touchObj.pageX - disx + "px";
      }
      if (JSON.stringify(binding.modifiers.vertical)) {
        //仅垂直拖拽
        moveObj.style.top = touchObj.pageY - disy + "px";
      }
      if (
        binding.modifiers.horizonta == undefined &&
        binding.modifiers.vertical == undefined
      ) {
        //不限制拖拽方向
        moveObj.style.left = touchObj.pageX - disx + "px";
        moveObj.style.top = touchObj.pageY - disy + "px";
      }
      e.preventDefault();
    });
  }
});

//将科学计数法转为字符串
Number.prototype.toUnExponential = function () {
  let num = this;
  //数值过大或过小，导致数值变成科学计数法
  if ((num + "").indexOf("e") > -1) {
    let numStr = num + "";
    let isMinus = ""; //判断是否为负数
    if (numStr.substring(0, 1) == "-") {
      numStr = numStr.substring(1);
      isMinus = "-";
    }

    let eIndex = numStr.indexOf("e");
    //截取尾数
    let num1 = numStr.substring(0, eIndex);
    //截取幂
    let num2 = numStr.substring(eIndex + 2);
    //从尾数中截取小数点后的位数
    let pointLen = num1.indexOf(".") > -1 ? num1.split(".")[1].length : 0;
    //需要补多少个0
    let zeroStr = "";

    if (numStr.indexOf("e-") > -1) {
      //数值小于1，则幂的次数就是数值前面0的数量，第一个0后面加“.”
      for (let i = 0; i < num2 - 1; i++) {
        zeroStr += "0";
      }
      return isMinus + "0." + zeroStr + num1.replace(".", "");
    } else {
      //数值大于1的，需要将幂的次数减去尾数中小数的位数
      for (let i = 0; i < num2 - pointLen; i++) {
        zeroStr += "0";
      }
      return isMinus + num1.replace(".", "") + zeroStr;
    }
  }
  return num;
};
//重写Number的toFixed方法
//digits：保留位数   resultPaddingZero：小数位不足是否补0（例：1.5保留两位，则四舍五入结果为2 ，结果返回2.00）
Number.prototype.toFixed = function (digits, resultPaddingZero) {
  //将科学计数法转为字符串
  let num = this.toUnExponential();
  // 是否为正数
  var isPositive = +num >= 0;
  num += "";

  // 去掉正负号，统一按照正数来处理，最后再加上符号
  num = num.replace(/^(?:-|\+)/gi, "");

  // 小数点过大
  /* istanbul ignore next */
  if (digits > 20 || digits < 0) {
    throw new RangeError("toFixed() digits argument must be between 0 and 20");
  }

  // 如果是简写如.11则整数位补0，变成0.11
  /* istanbul ignore next */
  if (/^\./gi.test(num)) {
    num = "0" + num;
  }

  // 非数字
  /* istanbul ignore next */
  if (!/^\d+\.?\d*$/gi.test(num)) {
    try {
      throw new Error("toFixed() num argument must be a valid num");
    } catch (err) {
      console.log(err);
    }
    return this;
  }

  var numParts = num.split(".");
  var result = "";

  // 在str后面加n个0
  var _paddingZero = function (str, n) {
    for (var i = 0; i < n; i++) {
      str += "0";
    }
    return str;
  };

  // 在str后面加0，直至str的长度达到n
  // 如果超过了n，则直接截取前n个字符串
  var _paddingZeroTo = function (str, n) {
    if (str.length >= n) {
      return str.substr(0, n);
    } else {
      return _paddingZero(str, n - str.length);
    }
  };

  // 直接就是整数
  if (numParts.length < 2) {
    result = numParts[0] + "." + _paddingZero("", digits);
    // 为浮点数
  } else {
    // 放大10的N次方倍
    var enlarge =
      numParts[0] +
      _paddingZeroTo(numParts[1], digits) +
      "." +
      numParts[1].substr(digits);
    // 取整
    enlarge = Math.round(enlarge) + "";
    // 缩小10的N次方
    while (enlarge.length <= digits) {
      enlarge = "0" + enlarge;
    }
    result =
      enlarge.substr(0, enlarge.length - digits) +
      "." +
      enlarge.substr(enlarge.length - digits);
  }

  // 如果最后一位为.,则去除
  result = result.replace(/\.$/gi, "").replace(/^\./gi, "0.");

  // 加上符号位
  result = isPositive ? Number(result) : Number(-result);
  //判断是否需要 小数位不足补0 的操作，如果本身值为0，则直接返回0而不是0.0
  if (resultPaddingZero == true && this != 0) {
    let decimalNum = (result + "").split(".")[1]; //截取小数点后面的数值
    let decimalLength = decimalNum ? decimalNum.length : 0; //获取小数位数
    for (let i = decimalLength; i < digits; i++) {
      if (i == 0) {
        result += ".";
      }
      result += "0";
    }
    return result;
  }
  return result.toUnExponential();
};

//指标取值的一些通用方法
Vue.prototype.$util = common;
