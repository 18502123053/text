import ResizeObserver from "resize-observer-polyfill";

export default {
  // 获取url参数
  request: function (paras) {
    var url = location.href;
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (var i = 0; i < paraString.length; i++) {
      var j = paraString[i];
      paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(
        j.indexOf("=") + 1,
        j.length
      );
    }
    var returnValue = paraObj[paras.toLowerCase()];
    if (typeof returnValue === "undefined") {
      return "";
    } else {
      return returnValue;
    }
  },
  /**
   * 校验只要是数字（包含正负整数，0以及正负浮点数）就返回true
   **/
  isNumber(val) {
    if (
      (val + "").trim() === "" ||
      isNaN(val + "") ||
      (typeof val != "string" && typeof val != "number")
    ) {
      return false;
    } else {
      return true;
    }
  },
  //判断是否为空
  isBlank: function (str) {
    return (
      str == undefined ||
      str == null ||
      str == "null" ||
      str === "" ||
      str == "-" ||
      str == "--" ||
      str == Infinity ||
      str == "/"
    );
  },

  /**
   * 数据取值过程报错时能返回空字符串
   * @param {Object} object 取值对象
   * @param {Array} fields 取值字段(按取值层级拼接数组)
   * @param {any} ifErr 指定取值过程出错返回的结果
   */
  fieldErr: function (object, fields, ifErr) {
    try {
      let resultObj = object;
      (fields || []).forEach((field, index) => {
        resultObj = resultObj[field];
      });
      return resultObj;
    } catch (err) {
      return ifErr || "";
    }
  },

  /**
   * 数据取值为空时能返回空字符串
   * @param {Object} object 取值对象
   * @param {Array} fields 取值字段(按取值层级拼接数组)
   * @param {any} ifErr 指定取值过程出错或为空返回的结果
   */
  ifnull: function (object, fields, ifErr) {
    try {
      let resultObj = object;
      (fields || []).forEach((field, index) => {
        resultObj = resultObj[field];
      });
      if (this.isBlank(resultObj)) {
        resultObj = ifErr || "";
      }
      return resultObj;
    } catch (err) {
      return ifErr || "";
    }
  },
  /**
   * 事件绑定的兼容写法
   * @param {Element} target html元素对象
   * @param {*} eventType 事件类型(click,touchmove等)
   * @param {*} handle 事件触发的内容
   */
  addEvents: function (target, eventType, handle) {
    if (target.addEventListener) {
      target.addEventListener(eventType, handle, false);
    } else if (target.attachEvent) {
      target.attachEvent("on" + eventType, handle);
    }
  },
  /**
   * 移除事件绑定兼容写法
   * @param {Element} target html元素对象
   * @param {*} eventType 事件类型(click,touchmove等)
   * @param {*} handle 事件触发的内容
   */
  removeEvents: function (target, eventType, handle) {
    if (target.removeEventListener) {
      target.removeEventListener(eventType, handle);
    } else if (target.detachEvent) {
      target.detachEvent("on" + eventType, handle);
    }
  },
  //获取元素样式属性
  getStyle: function (el, attr) {
    if (typeof window.getComputedStyle !== "undefined") {
      return window.getComputedStyle(el, null)[attr];
    } else if (typeof el.currentStyle !== "undefiend") {
      return el.currentStyle[attr];
    }
    return "";
  },

  //监听元素的resize-----------------------start

  addResizeListener(element, fn) {
    const resizeHandler = function (entries) {
      for (let entry of entries) {
        const listeners = entry.target.__resizeListeners__ || [];
        if (listeners.length) {
          listeners.forEach(fn => {
            fn(entry);
          });
        }
      }
    };

    if (!element.__resizeListeners__) {
      element.__resizeListeners__ = [];
      element.__ro__ = new ResizeObserver(resizeHandler);
      element.__ro__.observe(element);
    }
    element.__resizeListeners__.push(fn);
  }
  //监听元素的resize-----------------------end
};
