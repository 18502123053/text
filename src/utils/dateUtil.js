let SIGN_REGEXP = /([yMdhsmwqS])(\1*)/g;
let DEFAULT_PATTERN = "yyyy-MM-dd";
function padding(s, len) {
  len = len - (s + "").length;
  for (let i = 0; i < len; i++) {
    s = "0" + s;
  }
  return s;
}

export default {
  /**
   * 日期对象转字符串
   * @param {Date} date 日期对象
   * @param {String} pattern 格式字符串，为空作为默认yyyy-MM-dd
   * @description pattern属性中，可匹配上的格式字符为：y（年）、M（月）、d（日）、h（小时）、m（分钟）、s（秒）、S（毫秒）、w（周、1-7）、q（季度）
   *
   */
  format: function(date, pattern) {
    pattern = pattern || DEFAULT_PATTERN;
    return pattern.replace(SIGN_REGEXP, function($0) {
      switch ($0.charAt(0)) {
        case "y":
          return padding(date.getFullYear(), $0.length); //  年
        case "M":
          return padding(date.getMonth() + 1, $0.length); //  月
        case "d":
          return padding(date.getDate(), $0.length); //  日
        case "h":
          return padding(date.getHours(), $0.length); //  小时
        case "m":
          return padding(date.getMinutes(), $0.length); //  分钟
        case "s":
          return padding(date.getSeconds(), $0.length); //  秒
        case "S":
          return date.getMilliseconds(); //  毫秒
        case "w":
          return date.getDay() != 0 ? date.getDay() : 7; //  周
        case "q":
          return Math.floor((date.getMonth() + 3) / 3); //  季度
      }
    });
  },

  /**
   * 日期字符串转时间格式
   * @param {String} dateString 日期字符串
   * @param {Date} pattern 格式字符串, 必须与dateString的格式一致，不一致会返回null
   */
  parse: function(dateString, pattern) {
    let matchs1 = pattern.match(SIGN_REGEXP);
    let matchs2 = dateString.match(/(\d)+/g);
    if (matchs1.length == matchs2.length) {
      let _date = new Date(1970, 0, 1);
      for (let i = 0; i < matchs1.length; i++) {
        let _int = parseInt(matchs2[i]);
        let sign = matchs1[i];
        switch (sign.charAt(0)) {
          case "y":
            _date.setFullYear(_int);
            break;
          case "M":
            _date.setMonth(_int - 1);
            break;
          case "d":
            _date.setDate(_int);
            break;
          case "h":
            _date.setHours(_int);
            break;
          case "m":
            _date.setMinutes(_int);
            break;
          case "s":
            _date.setSeconds(_int);
            break;
          case "S":
            _date.setMilliseconds(_int);
            break;
        }
      }
      return _date;
    }
    return null;
  },

  /**
   * 将毫秒数时间戳转换成时间字符串
   * @param {Number} num 毫秒数时间戳
   * @param {String} pattern 格式字符串，为空则默认yyyy-mm-dd hh:mm:ss
   */
  formatByTimestamp: function(num, pattern) {
    let d = new Date();
    d.setTime(num);
    return this.format(d, pattern ? pattern : "yyyy-MM-dd hh:mm:ss");
  },

  /**
   * 获取日期所在周的开始结束时间（周一为第一天）
   * @param {Date} date 日期对象
   * @returns {Object} 返回起止时间字符串{start:Date, end:Date}
   */
  getWeekStartAndEnd: function(date) {
    // 起止日期数组
    let startStop = {};
    // 一天的毫秒数
    let millisecond = 1000 * 60 * 60 * 24;

    let currentDate = date;

    // 返回日期对应那天的星期数
    let week = currentDate.getDay();
    // 从周一到周日将星期数设为0-6
    let weekIndex = week != 0 ? week - 1 : 6;
    // 获得当前周的第一天
    let currentWeekFirstDay = new Date(
      currentDate.getTime() - millisecond * weekIndex
    );
    // 获得当前周的最后一天
    let currentWeekLastDay = new Date(
      currentWeekFirstDay.getTime() + millisecond * 6
    );
    // 添加至数组
    startStop.start = currentWeekFirstDay;
    startStop.end = currentWeekLastDay;

    return startStop;
  },

  /**
   * 获取日期所在月的开始结束时间
   * @param {Date} date
   * @returns {Object} 返回起止时间字符串{start:Date, end:Date}
   */
  getMonthStartAndEnd: function(date) {
    // 起止日期数组
    let startStop = {};
    let currentYear = date.getFullYear();
    let currentMonth = date.getMonth();
    // 获得当月的第一天
    let currentMonthFirstDay = new Date(currentYear, currentMonth, 1);
    // 获得当月的最后一天
    let currentMonthLastDay = new Date(currentYear, currentMonth + 1, 0);
    // 添加至数组
    startStop.start = currentMonthFirstDay;
    startStop.end = currentMonthLastDay;
    // 返回
    return startStop;
  },

  /**
   * 计算往前或往后几个月的日期
   * @param {*} date 日期参数
   * @param {*} num 月份差（正数为往后推num个月，负数往前推num个月）
   */
  getMonthLater: function(date, num) {
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    // 计算之后月份的最后一天,与当前日期比
    const result_date_last = new Date(year, month + num + 1, 0);
    const last_day = result_date_last.getDate();
    //例如当前为2020年3月31日，往前推1个月，为则为2020年2月31日，由于2月只有29天所以取2月最后一天
    if (last_day < day) {
      return result_date_last;
    }
    return new Date(year, month + num, day);
  },

  /**
   * 计算往前或往后几天的日期
   * @param {*} date 日期参数
   * @param {*} num 天数差（正数为往后推num天，负数往前推num天）
   */
  getDateLater: function(date, num) {
    return new Date(date.getTime() + 1000 * 60 * 60 * 24 * num);
  }
};
