// 提示标题
export const SUCCESS_TIP_TITLE = "成功";
export const WARNING_TIP_TITLE = "警告";
export const ERROR_TIP_TITLE = "错误";

// 动作提示
export const ADD_SUCCESS = "操作成功";
export const EDIT_SUCCESS = "操作成功";
export const REMOVE_SUCCESS = "操作成功";
export const REMOVE_FAILURE = "操作失败";
export const ASSIGN_SUCCESS = "操作成功";
export const UNASSIGN_SUCCESS = "操作成功";
export const COPY_SUCCESS = "操作成功";

export const SESSION_STORAGE_KEY = {
  //  用户
  USER: "API_USER",
  // 服务器时间
  SERVERDATE: "SERVER_DATE",
  //session更新时间
  UPDATE_TIME: "UPDATE_TIME"
};

export const SESSION_STORAGE_ACCOUNT = "ACCOUNT";

export const SESSION_STORAGE_BADGE = "BADGE";
