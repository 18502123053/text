/*
 * @Author: gonghairun
 * @Date: 2021-12-10 16:44:46
 * @LastEditors: gonghairun
 * @LastEditTime: 2021-12-13 16:07:16
 * @Description:
 */
import "./components/components";
// 引入echarts
import * as echarts from "echarts";
// 引入Element-UI
import ElementUI from "element-ui"; // 导入组件相关样式
Vue.use(ElementUI);

// 经营仓字体图标
import "@/assets/fonts/iconfont.css";
import Vue from "vue";
import App from "./App";
import router from "./router";

require("./utils/customPrototype.js");

Vue.prototype.$echarts = echarts;

Vue.config.productionTip = false;

import common from "./assets/js/common.js";
Vue.use(common);

new Vue({
  el: "#app",
  router,
  ...App
});
