FROM 10.88.20.88:10080/pub/nginx

ADD dist/ /usr/share/nginx/html/
ADD 404.html /usr/share/nginx/html/404.html
ADD nginx.conf  /etc/nginx/nginx.conf